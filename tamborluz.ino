#include <DigisparkRGB.h>

byte RED = 0;
byte BLUE = 4;
byte GREEN = 1;
byte COLORS[] = {RED, BLUE, GREEN};

int     piezoPin             = A0;  // NOTE:  THIS IS P5!
int     piezoVal             = 0;
int     piezoLowThreshold    = 80;
int     piezoHighThreshold   = 220;
bool    hitDetected          = false;
int     lightOn              = 0;  // 0: off, 1: low color, 2: high color
int64_t     cycleCount           = 0;  // count cycles since last hit detected, to prevent double hits
//int64_t one_second           = 16777216 / 128;
int64_t one_second           = 16777216 / 256;
int64_t     cycleLimit           = one_second / 200;  // prevent another hit detection for this many cycles.  90000 ~= 1 second
int64_t     piezoLowCycles       = one_second/120;  // how long to maintain low light flash
int64_t     piezoHighCycles      = one_second/100;  // how long to maintain high light flash
int     lightMode            = 0;


// the setup routine runs once when you press reset:
void setup()  { 
//  DigisparkRGBBegin();  // THIS IS NO GOOD, it assumes pins 0-2
  pinMode(RED, OUTPUT); 
  pinMode(GREEN, OUTPUT); 
  pinMode(BLUE, OUTPUT);
  pinMode(piezoPin, INPUT);
  setOff();

} 

void oloop() {
  cycleCount++;
  if (cycleCount > piezoLowCycles){
    cycleCount = 0;
    if (lightMode == 0){
      lightMode = 1;
      setLow();
    } else if (lightMode == 1){
      lightMode = 2;
      setHigh();
    } else {
      lightMode = 0;
      setOff();
    }
  }
}

void loop ()
{
  
  if(lightOn > 0){
    cycleCount += 1;
  }

  if(lightOn == 1){
    if (cycleCount > piezoLowCycles){
      cycleCount = 0;
      setOff();
    }
  }

  if(lightOn == 2){
    if (cycleCount > piezoHighCycles){
      cycleCount = 0;
      setOff();
    }
  }

  piezoVal = analogRead(piezoPin);
  if(lightOn < 2 && piezoVal > piezoHighThreshold){
    setHigh();
  } else if (lightOn < 1 && piezoVal > piezoLowThreshold){
    // all to low, but not off
    setLow();
  }
 

}

void setHigh(){
  analogWrite(RED, 255);
  analogWrite(BLUE, 0);
  analogWrite(GREEN, 0);
  lightOn = 2;
}

void setLow(){
  analogWrite(RED, 0);
  analogWrite(BLUE, 255);
  analogWrite(GREEN, 0);
  lightOn = 1;
}

void setOff(){
  analogWrite(RED, 0);
  analogWrite(GREEN, 0);
  analogWrite(BLUE, 0);
  lightOn = 0;
}

/*
void fade(byte Led, boolean dir)
{
  int i;

  //if fading up
  if (dir)
  {
    for (i = 0; i < 256; i++) 
    {
      DigisparkRGB(Led, i);
      DigisparkRGBDelay(25);//1);
    }
  }
  else
  {
    for (i = 255; i >= 0; i--) 
    {
      DigisparkRGB(Led, i);
      DigisparkRGBDelay(25);//1);
    }
  }
}
*/
