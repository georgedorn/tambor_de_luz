# tambor_de_luz

AT-Tiny-based (digispark) drum trigger for a portable drum.


Findings:

- whole thing can be driven via 12V - the digispark's Vin can take 7V - 30V and will regulate down fairly efficiently without too much waste heat.
-- there are purpose-built battery packs for LED strips that this could use.
- 5 meters of RGB LED tape at 12V maxes out around 4A.  PWM, strobing, and using colors other than white will reduce this significantly.
- using a full 5m at full brightness and 100% duty cycle should consume &LT;50W, so a "12V, 6000 mAh" battery should run everything for _at least_ 75 minutes. Realistically, 3+ hours should be easy.


Major problems to solve:

- how to store various light show patterns
- how to choose different light show patterns.  pushbutton?  dip switches?
- sane and reliable way to trigger on piezo input, preferably without polling.

Relying on the ATtiny85 means the common TimerOne library doesn't work.  Wrong size registers.
