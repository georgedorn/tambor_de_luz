*Polling the piezo*

I'm not sure polling is the best approach, but I have yet to find a reasonable way to configure an analog interrupt.
From https://www.sparkfun.com/tutorials/330 :

```
#define DRUM1 0    // Our analog pin

byte val = 0;      // Initializing the variable for the voltage value

void setup()
{
  Serial.begin(9600);  // Initializing the serial port at 9600 baud
}

void loop()
{
  val = analogRead(DRUM1);  // Read the voltage
  Serial.println(val, DEC); // Print the voltage to the terminal
}
```
Or this:
```
#define DRUM1 0
#define THRESHOLD 200

byte val = 0;

void setup()
{
  Serial.begin(9600);
}

void loop()
{
  val = analogRead(DRUM1);

  if(val > THRESHOLD) { Serial.println("HARD"); }
  else if(val > 0) { Serial.println("SOFT"); }
 
}
```

Another polling example here: http://www.spikenzielabs.com/SpikenzieLabs/DrumKitKit.html
This project suggests it can poll several piezos faster enough to detect hits.  It's using a standard arduino, and it also counts cycles between detections to prevent retriggers, suggesting _90_ cycles through `loop()` between triggers.  On a 16mHz processor, that's at least 5.5ms, probably much more.  But that timing should be consistent in any case.


*Debouncing*

A resistor parallel to the piezo is absolutely required.  Without it, the signal "sticks" high indefinitely after positive voltage applied.
It needs to be tied low to come back down, and the value of the resistor changes the behavior.  A low value (<1k) makes the drop off very rapid, but
makes it harder to detect hits as well.   This may be desirable, however.

Until the piezo is mounted on the drum, and possibly after, a 5meg potentiometer in series with a 1k resistor should be used.
Debouncing is likely to still be a problem, though repeated lights of the same color may not be noticable.  Alternately, ignoring hits for 15-25ms may help,
but timing is currently imprecise on the digispark due to not having found a working real-time timer library compatible with it.


*Measuring time above threshold*

http://todbot.com/blog/2006/10/29/spooky-arduino-projects-4-and-musical-arduino/
http://todbot.com/arduino/sketches/midi_drum_kit/midi_drum_kit.pde

This suggests that measuring the time the voltage stays above a threshold is a better approach to determining the velocity of a hit.
That approach is a lot more complex so save this in case simply reading mV isn't sufficient.

```
// in loop()
  // deal with first piezo, this is kind of a hack
  val = analogRead(piezoAPin);
  if( val >= PIEZOTHRESHOLD ) {
    t=0;
    while(analogRead(piezoAPin) >= PIEZOTHRESHOLD/2) {
      t++;
    }
    noteOn(drumchan,note_hihatopen, t*2);
    delay(t);
    noteOff(drumchan,note_hihatopen,0);
  }
```

*Setting up the piezo to interrupt*

https://forum.arduino.cc/index.php?topic=17450.0

This seems to suggest needing a lot of setup, including possibly external hardware?

Also this, which is specific to the ATtiny: https://www.youtube.com/watch?v=Cxs8O5C-700
The upside of this approach is that a potentiometer could be used as a voltage divider, to set the threshold on-the-fly.
The downside is it requires two pins, probably 0 and 1, which are the preferred pins for PWM as well...

I was previously incorrect about the severity of this problem, though - there are 6 pins available, not 5.  The comparator might work in conjunction with a button to select a "program".